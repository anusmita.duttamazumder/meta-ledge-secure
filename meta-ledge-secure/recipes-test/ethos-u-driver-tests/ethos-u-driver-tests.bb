DESCRIPTION = "Functional test for the Ethos-U NPU driver"

LICENSE = "MIT"

LIC_FILES_CHKSUM = "\
    file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
"

RDEPENDS:${PN} = "python3-core python3-fcntl"

SRC_URI = " \
    file://ethos-u-driver-tests.py \
"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ethos-u-driver-tests.py ${D}${bindir}
}
